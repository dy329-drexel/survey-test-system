Name:Deepak Yadav
dy329@drexel.edu




Introduction/Functional Description:
	Survey/Test taking system is a software written in Java using some of the software design patterns. We are using console input for the program and for the output we are giving user option to choose between console and audio.
Functional Description of the Survey System(I will test limited functionality of this software):
1)  The system should allow a survey/test to be entered, modified and saved to a file on disk.
2)   We can  load existing Survey/Test from files.
3)  It should allow Survey/Test to be  taken and tabulated/graded. And when taken you should store your responses along with the survey. 
4)The survey should be stored in a file system. Each survey should be stored in an individual file. 
5) A survey/test can be composed of any combination of these type of questions:  True/False, Multiple Choice, short answer, essay answer, matching, rank the choices.  
6) Each question can accept a single answer or if appropriate, multiple answers. But a True/False question would not accept multiple answers. 
7)  Each test question, aside from essays, should have some sort of correct answer. This implies that each question can therefore be graded. 
8) You should be able to modify all parts of the questions (i.e. the prompt, the choices or limit etc.) 
9)While tabulating your survey or test you should be able to see the number of responses to each question.


Sample Survey files: All survey will be stored in ./Surveys directory and All tests are in ./Tests directory
I used Seriliazation to save survey and test to file


Survey 1: with all type of questions
Sample 2: with one question added this to check functionality to load survey based on user's choice 

Sample Test Files
new test: All type of questions with correct answer
Test Sample 2: to check loading from files and loading test according to user's choice.

When user load and then display a survey or test it will ask about the way to display it
you can set console or audio output
and it will display accordingly
