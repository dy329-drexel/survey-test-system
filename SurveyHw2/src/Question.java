import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class Question implements java.io.Serializable{

	
	private static final long serialVersionUID = 2638976008505558019L;
	protected String prompt;
	protected Car Response;
	
	
	public void setPrompt(String ques){
		this.prompt=ques;
	}
	
	public String getPrompt(){
		return this.prompt;
	}
	
	public void displayPrompt(){
		System.out.println(this.prompt);
	}
	public void editPrompt(){
		System.out.println("Enter a new Prompt");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String pro=this.prompt;
		try {
			pro = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("invalid format");
		}
		
			this.prompt = pro;
		
	}
	public abstract void Display(outPut output);
	public abstract void create();
	public abstract void modify();

	public abstract Car take() ;
	
}
