
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;

import java.io.FileOutputStream;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;


public class Survey implements java.io.Serializable{

	private static final long serialVersionUID = -3080130933191194200L;

	public String name;
	 
	protected ArrayList<Question> questions;
	protected HashMap<String,ArrayList<Car>> responses;
	
	
	// default console output
	outPut output=new consoleO();


//Constructor
	
	public Survey()
	{
		name = null;
	
		questions = new ArrayList<Question>();
		responses=new HashMap<String,ArrayList<Car>>();
		
	}
	public void editQuestion(int promp){
		
		
		
		
		questions.get(promp).modify();;
		
		
	}
	
	public void setDisplayMethod(){
		System.out.println("how do you want to display \n 1)Cosole ouput 2)Audio output");
		Scanner in = new Scanner(System.in);
		int numinp = in.nextInt();
		if(numinp==1)
			output=new consoleO();
		else if (numinp==2)
			output=new audioO();
		else
		{
			System.out.println("wrong input");
			setDisplayMethod();
	}
		this.display();
		
	}
	public void display()
	{
		
		output.displaystring(this.name + "\n   ");
		
		
		
		for (int i = 0; i < questions.size(); i++)
		{
			output.displaystring((i + 1) + ":");
			questions.get(i).Display(output);
			
		}
	}
	
	
	public void createSurvey() throws IOException{
		System.out.println("enter name for survey");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		this.name = br.readLine();
	}
	
	
	//Adding question
	public void addQuestion(Question abc) throws Exception{
		abc.create();
		System.out.println("Press y to save ");
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		String res=null;
		try {
			res = br1.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(res.equals("y")){
		this.questions.add(abc);
		System.out.println("saved");
	}else
		return;
		
		
	}
	
//Saving to a files
	
	public void savetofile() throws Exception
	{
	
		
		File verifyFolder = new File("Surveys/");
		if (!verifyFolder.exists())
			verifyFolder.mkdirs();

		File createFile = new File("surveys/" + this.name );

		if (!createFile.exists())
			createFile.createNewFile();

		
		FileOutputStream fileOut = new FileOutputStream(createFile);
		ObjectOutputStream out = new ObjectOutputStream(fileOut);
		out.writeObject(this);
		
	

	
		System.out.println("File saved at surveys/" + this.name );

		//Close all the streams
		fileOut.close();
		out.close();
		
		out.close();
		
	
			}
	
	//Loading a survey
public Survey loadSurvey(String name) throws IOException, ClassNotFoundException
{

			FileInputStream fi=null;
		
		 fi = new FileInputStream("Surveys/" + name);
		ObjectInputStream ois = new ObjectInputStream(fi);
		
			Survey currentSurvey = (Survey) ois.readObject();
			ois.close();
			fi.close();
			return currentSurvey;
	}
public void take() {

	System.out.println(this.name + "\n   ");
	System.out.println("Enter your name");
	BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
	String taker = null;
	try {
		taker=br1.readLine();
	} catch (IOException e1) {
		take();
		e1.printStackTrace();
	}
	//responses.put(++number, new ArrayList<Car>());
	ArrayList<Car> respo=new ArrayList<Car>();
	for (int i = 0; i < questions.size(); i++)
	{
		System.out.print((i + 1) + ":");
		questions.get(i).Display(output);
		
			respo.add(questions.get(i).take());
			
			
		
		
		
	}
	
	responses.put(taker, respo);

try {
	this.savetofile();
} catch (Exception e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
	
}


		
		
		
		
		
	
	
	
	
	
	
	

public void  tabulate()
{
	ArrayList<HashMap<Car,Integer>> tempArr = new ArrayList<HashMap<Car,Integer>>();
	
	for(int i = 0; i < questions.size(); i++)
	{
		
		HashMap<Car, Integer> temp = new HashMap<Car,Integer>();
		for(Entry<String,ArrayList<Car>> entry :  responses.entrySet())
		{
			if(!temp.containsKey(entry.getValue().get(i)))
			{
				temp.put(entry.getValue().get(i), 1);
			}
			else
			{
				temp.put(entry.getValue().get(i), (temp.get(entry.getValue())+1));
			}
			
		}
		tempArr.add(temp);
	}
	for(int k=0;k<tempArr.size();k++){
		questions.get(k).displayPrompt();
		HashMap<Car,Integer> abc=tempArr.get(k);
	 for(Entry<Car,Integer> entry :  abc.entrySet())
	    {   //print keys and values
		 entry.getKey().Display();
		System.out.println("  "+" "+entry.getValue());
	    }
	}
}
public int[] grade() {
	// TODO Auto-generated method stub
	return null;
}

public boolean checkkey(HashMap<Car, Integer> tempHash,Car a)
{
	
	Integer value = tempHash.get(a);
	if (value != null) {
	    return false;
	} else {
	    return true;
	}

}
}
